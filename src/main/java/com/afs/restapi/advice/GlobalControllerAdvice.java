package com.afs.restapi.advice;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.DepartmentNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler({EmployeeNotFoundException.class, CompanyNotFoundException.class, DepartmentNotFoundException.class})
    //add target exception here
    @ResponseStatus(HttpStatus.NOT_FOUND) // return 404
    public ErrorResponse handleNotFoundException(Exception exception) {
        return new ErrorResponse(HttpStatus.NOT_FOUND.value(), exception.getMessage());
    }
}
