package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("departments")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @PostMapping("/{id}/employees")
    public Department addEmployeeToDepartment(@PathVariable Long id, @RequestBody Employee employee) {
        return departmentService.addEmployeeToDepartment(id, employee);
    }

    @DeleteMapping("/{id}/employees/{employeeId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeEmployeeFromDepartment(@PathVariable Long id, @PathVariable Long employeeId) {
        departmentService.removeEmployeeFromDepartment(id, employeeId);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByDepartmentId(@PathVariable Long id) {
        return departmentService.getEmployeesByDepartmentId(id);
    }

    @GetMapping("/{id}/company")
    public Company getCompanyByDepartmentId(@PathVariable Long id) {
        return departmentService.getCompanyByDepartmentId(id);
    }
}
