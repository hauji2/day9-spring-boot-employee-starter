package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.DepartmentNotFoundException;
import com.afs.restapi.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;

    public Department findById(Long id) {
        return departmentRepository.findById(id).orElseThrow(DepartmentNotFoundException::new);
    }

    public Department addEmployeeToDepartment(Long departmentId, Employee employee) {
        Department department = findById(departmentId);
        department.getEmployees().add(employee);
        return departmentRepository.save(department);
    }

    public void removeEmployeeFromDepartment(Long departmentId, Long employeeId) {
        Department department = findById(departmentId);
        department.getEmployees().removeIf(employee -> employee.getId().equals(employeeId));
        departmentRepository.save(department);
    }

    public List<Employee> getEmployeesByDepartmentId(Long id) {
        Department department = findById(id);
        return department.getEmployees();
    }

    public Company getCompanyByDepartmentId(Long id) {
        Department department = findById(id);
        return department.getCompany();
    }
}
