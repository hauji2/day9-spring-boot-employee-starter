package com.afs.restapi.exception;

public class DepartmentNotFoundException extends RuntimeException {
    public DepartmentNotFoundException() {
        super("department id not found");
    }
}
