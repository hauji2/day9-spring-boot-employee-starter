package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class CompanyApiTest {

    @Autowired
    private MockMvc mockMvc;

    @SpyBean
    CompanyRepository companyRepository;

    @SpyBean
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        companyRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        Company previousCompany = new Company(1L, "abc");
        Company savedCompany = companyRepository.save(previousCompany);

        Company companyUpdateRequest = new Company(1L, "xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", savedCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(status().is(204));

        Optional<Company> optionalCompany = companyRepository.findById(savedCompany.getId());
        assertTrue(optionalCompany.isPresent());
        Company updatedCompany = optionalCompany.get();
        assertEquals(savedCompany.getId(), updatedCompany.getId());
        assertEquals(companyUpdateRequest.getName(), updatedCompany.getName());
    }

    @Test
    void should_not_update_company_name() throws Exception {
        Company companyUpdateRequest = new Company(1L, "xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", 99L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("company id not found"));
    }

    @Test
    void should_delete_company_name() throws Exception {
        Company company = new Company(1L, "abc");
        Company savedCompany = companyRepository.save(company);

        mockMvc.perform(delete("/companies/{id}", savedCompany.getId()))
                .andExpect(status().is(204));

        assertTrue(companyRepository.findById(savedCompany.getId()).isEmpty());
    }

    @Test
    void should_create_employee() throws Exception {
        Company company = getCompany1();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(company);
        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyRequest))
                .andExpect(status().is(201))
                .andExpect(result -> {
                    Company createdCompany = objectMapper.readValue(result.getResponse().getContentAsString(),
                            Company.class);
                    assertEquals(company.getName(), createdCompany.getName());
                    assertNotNull(createdCompany.getId());
                });
    }

    @Test
    void should_find_companies() throws Exception {
        Company company = getCompany1();
        Company savedCompany = companyRepository.save(company);

        mockMvc.perform(get("/companies"))
                .andExpect(status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company.getName()));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        Company company1 = getCompany1();
        Company company2 = getCompany2();
        Company company3 = getCompany3();

        Company savedCompany1 = companyRepository.save(company1);
        Company savedCompany2 = companyRepository.save(company2);
        companyRepository.save(company3);

        mockMvc.perform(get("/companies")
                        .param("pageNumber", "1")
                        .param("pageSize", "2"))
                .andExpect(status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedCompany1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(savedCompany2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(company2.getName()))
        ;
    }

    @Test
    void should_find_company_by_id() throws Exception {
        Company company = getCompany1();
        Employee employee = getEmployee();
        Department department = new Department();
        department.getEmployees().add(employee);
        company.getDepartments().add(department);
        Company savedCompany = companyRepository.save(company);

        System.out.println(savedCompany);

        MvcResult mvcResult = mockMvc.perform(get("/companies/{id}", savedCompany.getId()))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        Company foundCompany = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Company.class);

        assertEquals(savedCompany.getId(), foundCompany.getId());
        assertEquals(company.getName(), foundCompany.getName());
        assertEquals(1, foundCompany.getDepartments().size());
        assertEquals(1, foundCompany.getDepartments().get(0).getEmployees().size());

        Employee foundEmployee = foundCompany.getDepartments().get(0).getEmployees().get(0);
        assertEquals(employee.getId(), foundEmployee.getId());
        assertEquals(employee.getName(), foundEmployee.getName());
        assertEquals(employee.getAge(), foundEmployee.getAge());
        assertEquals(employee.getGender(), foundEmployee.getGender());
        assertEquals(employee.getSalary(), foundEmployee.getSalary());
    }

    @Test
    void should_not_find_company_by_id() throws Exception {
        mockMvc.perform(get("/companies/{id}", 1L))
                .andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("company id not found"));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        // Initialize your entities and save Company
        Company company = getCompany1();
        Employee employee = getEmployee();
        Department department = new Department();
        department.getEmployees().add(employee);
        department.setCompany(company);
        company.getDepartments().add(department);
        Company savedCompany = companyRepository.save(company);

        MvcResult mvcResult = mockMvc.perform(get("/companies/{companyId}/employees", savedCompany.getId()))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        JavaType type = objectMapper.getTypeFactory().constructCollectionType(List.class, Employee.class);
        List<Employee> foundEmployees = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), type);

        assertEquals(1, foundEmployees.size());
        Employee foundEmployee = foundEmployees.get(0);
        assertEquals(employee.getId(), foundEmployee.getId());
        assertEquals(employee.getName(), foundEmployee.getName());
        assertEquals(employee.getAge(), foundEmployee.getAge());
        assertEquals(employee.getGender(), foundEmployee.getGender());
        assertEquals(employee.getSalary(), foundEmployee.getSalary());
    }

    @Test
    void should_not_find_employees_by_companies_when_company_not_found() throws Exception {
        mockMvc.perform(get("/companies/{companyId}/employees", 1L))
                .andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("company id not found"));
    }


    private static Employee getEmployee() {
        Employee employee = new Employee();
        employee.setName("Bob");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        return employee;
    }


    private static Company getCompany1() {
        Company company = new Company();
        company.setName("ABC");
        return company;
    }

    private static Company getCompany2() {
        Company company = new Company();
        company.setName("DEF");
        return company;
    }

    private static Company getCompany3() {
        Company company = new Company();
        company.setName("XYZ");
        return company;
    }
}