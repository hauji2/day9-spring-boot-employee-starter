package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.DepartmentRepository;
import com.afs.restapi.service.DepartmentService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class DepartmentApiTest {
    @SpyBean
    DepartmentRepository departmentRepository;

    @SpyBean
    DepartmentService departmentService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @SpyBean
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        departmentRepository.deleteAll();
        companyRepository.deleteAll();
    }

    @Test
    void should_return_department_with_employees_when_adding_employee_to_department() throws Exception {
        Department department = new Department(1L);
        Department saveDepartment = departmentRepository.save(department);

        Employee employee = new Employee(1L, "John Doe", 30, "Male", 1000);

        mockMvc.perform(post("/departments/{departmentId}/employees", saveDepartment.getId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(employee)))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(result -> {
                    Department department1 = objectMapper.readValue(result.getResponse().getContentAsString(), Department.class);
                    Employee employee1 = department1.getEmployees().get(0);

                    assertEquals(employee.getName(), employee1.getName());
                    assertEquals(employee.getAge(), employee1.getAge());
                    assertEquals(employee.getGender(), employee1.getGender());
                    assertEquals(employee.getSalary(), employee1.getSalary());
                });
    }

    @Test
    void should_return_404_when_adding_employee_to_department_given_department_not_found() throws Exception {
        Department department = new Department(1L);
        departmentRepository.save(department);

        Employee employee = new Employee(1L, "John Doe", 30, "Male", 1000);
        mockMvc.perform(post("/departments/{departmentId}/employees", 99L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(employee)))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(jsonPath("$.message").value("department id not found"));
    }

    @Test
    void should_remove_employee_from_department() throws Exception {
        Department department = new Department(1L);
        Employee employee = new Employee(1L, "John Doe", 30, "Male", 1000);
        department.getEmployees().add(employee);
        Department savedDepartment = departmentRepository.save(department);

        mockMvc.perform(delete("/departments/{departmentId}/employees/{employeeId}", savedDepartment.getId(), savedDepartment.getEmployees().get(0).getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Department updatedDepartment = departmentRepository.findById(savedDepartment.getId()).get();

        assertTrue(updatedDepartment.getEmployees().isEmpty());
    }

    @Test
    void should_return_404_when_removing_employee_from_department_given_department_not_found() throws Exception {
        Employee employee = new Employee(1L, "John Doe", 30, "Male", 1000);

        mockMvc.perform(delete("/departments/{departmentId}/employees/{employeeId}", 99L, employee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(jsonPath("$.message").value("department id not found"));
    }

    @Test
    void should_return_all_employees_in_department() throws Exception {
        Department department = new Department(1L);
        Employee savedEmployee = new Employee(null, "John Doe", 30, "Male", 1000);
        Employee savedEmployee2 = new Employee(null, "Jane Doe", 30, "Female", 1000);
        department.getEmployees().add(savedEmployee);
        department.getEmployees().add(savedEmployee2);
        Department savedDepartment = departmentRepository.save(department);

        mockMvc.perform(get("/departments/{departmentId}/employees", savedDepartment.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(result -> {
                    List<Employee> employees = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
                    });
                    assertEquals(2, employees.size());
                    assertEquals(savedEmployee.getName(), employees.get(0).getName());
                    assertEquals(savedEmployee2.getName(), employees.get(1).getName());
                });
    }

    @Test
    void should_return_404_when_getting_employees_in_department_given_department_not_found() throws Exception {
        mockMvc.perform(get("/departments/{departmentId}/employees", 99L))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(jsonPath("$.message").value("department id not found"));
    }

    @Test
    void should_return_company_when_getting_department_by_id() throws Exception {
        Company company = new Company(1L, "ABC");
        Department department = new Department(1L);
        company.getDepartments().add(department);
        department.setCompany(company);
        company.getDepartments().add(department);
        Company savedCompany = companyRepository.saveAndFlush(company);

        mockMvc.perform(get("/departments/{id}/company", savedCompany.getDepartments().get(0).getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(result -> {
                    Company company1 = objectMapper.readValue(result.getResponse().getContentAsString(), Company.class);
                    assertEquals(company.getName(), company1.getName());
                });
    }

    @Test
    void should_return_404_when_getting_company_by_id_given_department_not_found() throws Exception {
        mockMvc.perform(get("/departments/{id}/company", 99L))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(jsonPath("$.message").value("department id not found"));
    }
}
